package org.zapto.raulrm00.gamingnow.pairing.models;

import org.zapto.raulrm00.gamingnow.rawg.util.APIGameGetter;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class Room {
    private UUID id;
    private String game;
    private boolean valid;
    private long clients_number;

    public boolean isValid() {
        return valid;
    }

    public long getClientsNumber() {
        return clients_number;
    }

    public String getGame() {
        return game;
    }

    public String getGameTitle() {
        try {
            return new APIGameGetter().execute(game).get().getName();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return "No name found";
    }
}
