package org.zapto.raulrm00.gamingnow.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.zapto.raulrm00.gamingnow.R;

public class SearchActivity extends AppCompatActivity {

    private EditText query;

    public void onSearch(View view) {
        String text = query.getText().toString().trim();
        if (!text.equals("")) {
            Intent intent = new Intent(this, SearchResultActivity.class);
            intent.putExtra("original", text);
            intent.putExtra("query", text.replace(' ', '-'));
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.search_no_text), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.action_search));
        query = findViewById(R.id.search_text);
    }
}
