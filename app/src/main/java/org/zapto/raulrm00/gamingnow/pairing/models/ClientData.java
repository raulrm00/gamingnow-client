package org.zapto.raulrm00.gamingnow.pairing.models;

import java.util.UUID;

public class ClientData {
    private UUID id;
    private String username;
    private String platform;

    public String getUsername() {
        return username;
    }

    public String getPlatform() {
        return platform;
    }
}
