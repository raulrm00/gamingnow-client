package org.zapto.raulrm00.gamingnow.rawg.models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Game {
    private String name;
    private String slug;
    private String description_raw;
    private Date released;
    private String background_image;
    private double rating;
    private double rating_top;
    private int metacritic;
    private Genre[] genres;

    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getImageUrl() {
        return background_image;
    }

    public String getDescription() {
        return description_raw;
    }

    public String getRating() {
        return rating + " / " + rating_top;
    }

    public String getDate() {
        return released == null ? "No info" : format.format(released);
    }

    public Genre[] getGenres() {
        return genres;
    }

    public String getGenresString() {
        StringBuilder cadena = new StringBuilder();
        for (Genre g : genres) {
            cadena.append(g.getName()).append(", ");
        }
        if (cadena.length() > 0) {
            cadena.delete(cadena.length() - 2, cadena.length());
        }
        return cadena.toString();
    }

    public int getMetacritic() {
        return metacritic;
    }
}
