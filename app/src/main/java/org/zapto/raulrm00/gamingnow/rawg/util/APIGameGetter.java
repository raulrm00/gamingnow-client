package org.zapto.raulrm00.gamingnow.rawg.util;

import android.os.AsyncTask;

import org.zapto.raulrm00.gamingnow.rawg.API;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;

public class APIGameGetter extends AsyncTask<String, Void, Game> {
    @Override
    protected Game doInBackground(String... games) {
        return API.getGame(games[0]);
    }
}
