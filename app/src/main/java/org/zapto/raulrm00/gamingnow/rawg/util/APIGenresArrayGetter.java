package org.zapto.raulrm00.gamingnow.rawg.util;

import android.os.AsyncTask;

import org.zapto.raulrm00.gamingnow.rawg.API;
import org.zapto.raulrm00.gamingnow.rawg.models.Genre;

public class APIGenresArrayGetter extends AsyncTask<Void, Void, Genre[]> {
    @Override
    protected Genre[] doInBackground(Void... voids) {
        return API.getGenres();
    }
}
