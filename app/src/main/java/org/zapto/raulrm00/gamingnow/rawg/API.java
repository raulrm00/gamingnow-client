package org.zapto.raulrm00.gamingnow.rawg;

import com.google.gson.Gson;

import org.zapto.raulrm00.gamingnow.rawg.models.Game;
import org.zapto.raulrm00.gamingnow.rawg.models.GameResult;
import org.zapto.raulrm00.gamingnow.rawg.models.Genre;
import org.zapto.raulrm00.gamingnow.rawg.models.GenresResult;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class API {

    private static final String GAME_QUERY_FORMAT = "https://api.rawg.io/api/games?search=%s&page_size=%d&page=%d";
    private static final String GAME_DETAILS_QUERY_FORMAT = "https://api.rawg.io/api/games/%s";
    private static final String GAMES_FROM_GENRE = "https://api.rawg.io/api/games?genres=%s&page_size=%d&page=%d";
    private static final String MOST_POPULAR_GAMES_FROM_YEAR = "https://api.rawg.io/api/games?dates=%d-01-01,%d-12-31&ordering=-added&page_size=%d&page=%d";  // Las fechas se introducen en formato yyyy-MM-dd
    private static final String HIGHEST_RATED_GAMES_FROM_YEAR = "https://api.rawg.io/api/games?dates=%d-01-01,%d-12-31&ordering=-rating&page_size=%d&page=%d";
    private static final String GENRES_LIST = "https://api.rawg.io/api/genres";
    private static final String GENRE_DETAILS = "https://api.rawg.io/api/genres/%s";

    private static String getJson(String query) {
        String json = null;
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(query);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("User-Agent", "Gaming Now!/0.1 (Android)");
            if (connection.getResponseCode() != 200) {
                throw new RuntimeException("ERROR " + connection.getResponseCode());
            }
            InputStream inStream = connection.getInputStream();
            json = new Scanner(inStream, "UTF-8").useDelimiter("\\Z").next();
        } catch (RuntimeException | IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return json;
    }

    public static Genre[] getGenres() {
        String json = getJson(GENRES_LIST);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        GenresResult result = gson.fromJson(json, GenresResult.class);
        return result.getResults();
    }

    public static Genre getGenre(String slug) {
        String query = String.format(GENRE_DETAILS, slug);
        String json = getJson(query);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        Genre result = gson.fromJson(json, Genre.class);
        return result;
    }

    public static Game getGame(String slug) {
        String query = String.format(GAME_DETAILS_QUERY_FORMAT, slug);
        String json = getJson(query);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        Game result = gson.fromJson(json, Game.class);
        return result;
    }

    public static Game[] getGamesFromQuery(String name, int page_size, int page) {
        String query = String.format(GAME_QUERY_FORMAT, name, page_size, page);
        String json = getJson(query);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        GameResult result = gson.fromJson(json, GameResult.class);
        return result.getGames();
    }

    public static Game[] getGamesFromGenre(int page_size, int page, String... genreSlug) {
        StringBuilder cadena = new StringBuilder();
        for (String genero : genreSlug) {
            cadena.append(genero).append(",");
        }
        cadena.deleteCharAt(cadena.length() - 1);
        String query = String.format(GAMES_FROM_GENRE, cadena.toString().trim(), page_size, page);
        String json = getJson(query);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        GameResult result = gson.fromJson(json, GameResult.class);
        return result.getGames();
    }

    public static Game[] getMostPopularGamesFromYear(int year, int page_size, int page) {
        String query = String.format(MOST_POPULAR_GAMES_FROM_YEAR, year, year, page_size, page);
        String json = getJson(query);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        GameResult result = gson.fromJson(json, GameResult.class);
        return result.getGames();
    }

    public static Game[] getHighestRatedGamesFromYear(int year, int page_size, int page) {
        String query = String.format(HIGHEST_RATED_GAMES_FROM_YEAR, year, year, page_size, page);
        String json = getJson(query);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        GameResult result = gson.fromJson(json, GameResult.class);
        return result.getGames();
    }
}
