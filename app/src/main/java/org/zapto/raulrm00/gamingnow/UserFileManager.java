package org.zapto.raulrm00.gamingnow;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import org.zapto.raulrm00.gamingnow.ui.login.data.model.LoggedInUser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class UserFileManager {

    private static final String FILE_NAME = "userData";

    private final Context context;

    public UserFileManager(Context context) {
        this.context = context;
    }

    public LoggedInUser read() {
        createFileIfNotExists();
        try {
            FileInputStream fis = context.openFileInput(FILE_NAME);
            InputStreamReader inputStreamReader =
                    new InputStreamReader(fis, StandardCharsets.UTF_8);
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = reader.readLine();
            }
            if (stringBuilder.toString().trim().length() == 0) {
                return null;
            }
            return new Gson().fromJson(stringBuilder.toString(), LoggedInUser.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void write(@Nullable LoggedInUser user) {
        createFileIfNotExists();
        String fileContents;
        if (user == null) {
            fileContents = "";
        } else {
            fileContents = new Gson().toJson(user);
        }
        try (FileOutputStream fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)) {
            fos.write(fileContents.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isLoggedIn() {
        return read() != null;
    }

    private void createFileIfNotExists() {
        try {
            File file = new File(context.getFilesDir(), FILE_NAME);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
