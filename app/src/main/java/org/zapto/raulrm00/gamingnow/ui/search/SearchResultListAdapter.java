package org.zapto.raulrm00.gamingnow.ui.search;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.GameDetailsActivity;
import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;
import org.zapto.raulrm00.gamingnow.ui.home.HomeListAdapter;
import org.zapto.raulrm00.gamingnow.rawg.util.APISearchGameArrayGetter;
import org.zapto.raulrm00.gamingnow.util.ImageDownloader;

import java.util.ArrayList;
import java.util.List;

public class SearchResultListAdapter extends RecyclerView.Adapter<HomeListAdapter.GameHolder> {

    private RecyclerView lista;

    private List<Game> juegos;
    private int page_size;
    private int page;
    private String query;

    public SearchResultListAdapter(RecyclerView lista, String query) {
        this.lista = lista;
        this.query = query;
        juegos = new ArrayList<>();
        page_size = 10;
        page = 1;
        init();
    }

    private void init() {
        new APISearchGameArrayGetter(page_size, page, query).next(juegos);
        notifyItemRangeInserted(0, 10);
        page++;
    }

    @NonNull
    @Override
    public HomeListAdapter.GameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_row, parent, false);
        HomeListAdapter.GameHolder holder = new HomeListAdapter.GameHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.GameHolder holder, final int position) {
        if (position == getItemCount() - 1) {
            new APISearchGameArrayGetter(page_size, page, query).next(juegos);
            lista.post(new Runnable() {
                @Override
                public void run() {
                    notifyItemRangeInserted(position, juegos.size() - position - 1);
                }
            });
            page++;
        }
        final Game game = juegos.get(position);
        ImageDownloader.DownloadImageFromInternet(game.getImageUrl(), holder.game_image);
        holder.game_title.setText(game.getName());
        holder.game_genres.setText(game.getGenresString());
        holder.game_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GameDetailsActivity.class);
                intent.putExtra("slug", game.getSlug());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return juegos.size();
    }
}
