package org.zapto.raulrm00.gamingnow.rawg.models;

public class Genre {
    private int id;
    private String name;
    private String slug;
    private int games_count;
    private String image_background;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getGamesCount() {
        return games_count + " games";
    }

    public String getImageBackground() {
        return image_background;
    }
}
