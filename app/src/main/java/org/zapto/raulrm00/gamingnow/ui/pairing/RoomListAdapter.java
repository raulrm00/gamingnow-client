package org.zapto.raulrm00.gamingnow.ui.pairing;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.RoomDetailsActivity;
import org.zapto.raulrm00.gamingnow.pairing.models.Room;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.RoomHolder> {
    private final List<Room> rooms;

    public RoomListAdapter() {
        rooms = new ArrayList<>();
    }

    @NonNull
    @Override
    public RoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.room_row, parent, false);
        return new RoomHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomHolder holder, int position) {
        final Room room = rooms.get(position);
        if (room.isValid()) {
            holder.room_valid.setVisibility(View.VISIBLE);
        } else {
            holder.room_invalid.setVisibility(View.VISIBLE);
        }
        holder.room_title.setText(room.getGameTitle());
        holder.room_clients_number.setText(String.valueOf(room.getClientsNumber()));
        holder.room_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RoomDetailsActivity.class);
                intent.putExtra("slug", room.getGame());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public void addRooms(Room[] rooms) {
        Collections.addAll(this.rooms, rooms);
        notifyItemRangeInserted(0, rooms.length);
    }

    public static class RoomHolder extends RecyclerView.ViewHolder {

        public RelativeLayout room_line;
        public TextView room_title, room_valid, room_invalid, room_clients_number;

        public RoomHolder(@NonNull RelativeLayout layout) {
            super(layout);
            room_line = layout.findViewById(R.id.room_line);
            room_title = layout.findViewById(R.id.room_title);
            room_valid = layout.findViewById(R.id.room_valid);
            room_invalid = layout.findViewById(R.id.room_invalid);
            room_clients_number = layout.findViewById(R.id.room_clients_number);
        }
    }
}
