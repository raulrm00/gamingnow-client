package org.zapto.raulrm00.gamingnow;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;

import org.zapto.raulrm00.gamingnow.pairing.services.PairingPOSTIntentService;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;
import org.zapto.raulrm00.gamingnow.rawg.models.Genre;
import org.zapto.raulrm00.gamingnow.ui.favorites.FavoritesDB;
import org.zapto.raulrm00.gamingnow.ui.genres.GenresResultActivity;
import org.zapto.raulrm00.gamingnow.rawg.util.APIGameGetter;
import org.zapto.raulrm00.gamingnow.ui.login.LoginActivity;
import org.zapto.raulrm00.gamingnow.ui.login.data.model.LoggedInUser;
import org.zapto.raulrm00.gamingnow.ui.pairing.PairingFragment;
import org.zapto.raulrm00.gamingnow.util.ImageDownloader;

import java.util.concurrent.ExecutionException;

public class GameDetailsActivity extends AppCompatActivity {

    private static final String PAIRING_SERVER_IP = "raulrm00.zapto.org";
    private static final String JOIN_ROOM_URL = String.format("http://%s/api/rooms/join/", PAIRING_SERVER_IP);

    private ImageButton favoritos;

    private boolean esFavorito;
    private FavoritesDB db;

    private Game juego;

    public void botonFavorito(View view) {
        if (esFavorito) {
            db.delete(juego.getSlug());
            favoritos.setImageResource(R.drawable.ic_favorite_border);
            esFavorito = false;
        } else {
            db.save(juego.getSlug());
            favoritos.setImageResource(R.drawable.ic_favorite);
            esFavorito = true;
        }
    }

    public void pairButton(View view) {
        PendingIntent pendingResult = createPendingResult(
                PairingFragment.PAIRING_REQUEST_CODE, new Intent(), 0);
        Intent intent = new Intent(this, PairingPOSTIntentService.class);
        intent.putExtra(PairingPOSTIntentService.URL_EXTRA, JOIN_ROOM_URL + juego.getSlug());
        intent.putExtra(PairingPOSTIntentService.WRITE_DATA, new Gson().toJson(LoginActivity.user, LoggedInUser.class));
        intent.putExtra(PairingPOSTIntentService.PENDING_RESULT_EXTRA, pendingResult);
        startService(intent);
        Toast.makeText(this, "" + getString(R.string.game_details_sent_pairing) + juego.getName() + ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_details);
        Toolbar toolbar = findViewById(R.id.game_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView nombre = findViewById(R.id.nombre_juego);
        TextView descricion = findViewById(R.id.descripcion_juego);
        TextView nota = findViewById(R.id.nota_juego);
        TextView fecha = findViewById(R.id.fecha_juego);
        TextView metacritic = findViewById(R.id.metacritic_juego);
        ImageView imagen = findViewById(R.id.imagen_juego);
        favoritos = findViewById(R.id.favoritos_juego);
        APIGameGetter request = new APIGameGetter();
        Game juego = null;
        try {
            juego = request.execute(getIntent().getStringExtra("slug")).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if (juego == null) {
            return;
        }
        this.juego = juego;
        LinearLayout generos = findViewById(R.id.generos_juego);
        LinearLayout fila = new LinearLayout(this);
        fila.setOrientation(LinearLayout.HORIZONTAL);
        generos.addView(fila);
        int count = 0;
        int child = 1;
        for (final Genre genero : juego.getGenres()) {
            if (count == 3) {
                generos.addView(new LinearLayout(this));
                fila = (LinearLayout) generos.getChildAt(child);
                count = 0;
                child++;
            }
            Button b = new Button(this);
            b.setText(genero.getName());
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), GenresResultActivity.class);
                    intent.putExtra("slug", genero.getSlug());
                    intent.putExtra("name", genero.getName());
                    v.getContext().startActivity(intent);
                }
            });
            fila.addView(b);
            count++;
        }
        descricion.setText(juego.getDescription());
        nota.setText(juego.getRating());
        fecha.setText(juego.getDate());
        metacritic.setText(String.valueOf(juego.getMetacritic()));
        nombre.setText(juego.getName());
        setTitle(juego.getName());
        ImageDownloader.DownloadImageFromInternet(juego.getImageUrl(), imagen);
        db = new FavoritesDB(this);
        esFavorito = false;
        if (db.isFavorite(juego.getSlug())) {
            favoritos.setImageResource(R.drawable.ic_favorite);
            esFavorito = true;
        }
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }
}
