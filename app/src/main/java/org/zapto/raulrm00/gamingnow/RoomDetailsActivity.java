package org.zapto.raulrm00.gamingnow;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.zapto.raulrm00.gamingnow.pairing.models.ClientData;
import org.zapto.raulrm00.gamingnow.pairing.services.PairingGETIntentService;
import org.zapto.raulrm00.gamingnow.ui.login.data.model.LoggedInUser;
import org.zapto.raulrm00.gamingnow.ui.pairing.ClientDataListAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RoomDetailsActivity extends AppCompatActivity {

    private static final String PAIRING_SERVER_IP = "raulrm00.zapto.org";
    private static final String STORED_ROOM_CLIENTS = String.format("http://%s/api/rooms/", PAIRING_SERVER_IP);
    private static final int ROOM_DATA_REQUEST_CODE = 13;

    private RecyclerView dataList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_details);
        Toolbar toolbar = findViewById(R.id.room_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.room_details_title));
        dataList = findViewById(R.id.client_data_list);
        adapter = new ClientDataListAdapter();
        layoutManager = new LinearLayoutManager(this);
        dataList.setAdapter(adapter);
        dataList.setLayoutManager(layoutManager);
        getUsersFromServer();
    }

    private void getUsersFromServer() {
        PendingIntent pendingResult = createPendingResult(
                ROOM_DATA_REQUEST_CODE, new Intent(), 0);
        Intent intent = new Intent(this, PairingGETIntentService.class);
        intent.putExtra(PairingGETIntentService.URL_EXTRA, STORED_ROOM_CLIENTS + getIntent().getStringExtra("slug") + "/clients");
        intent.putExtra(PairingGETIntentService.PENDING_RESULT_EXTRA, pendingResult);
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ROOM_DATA_REQUEST_CODE) {
            switch (resultCode) {
                case PairingGETIntentService.ERROR_CODE:
                    Toast.makeText(this, getString(R.string.room_details_error), Toast.LENGTH_LONG);
                    break;
                case PairingGETIntentService.RESULT_CODE:
                    LoggedInUser[] users = new Gson().fromJson(data.getStringExtra(PairingGETIntentService.JSON_RESULT_EXTRA), LoggedInUser[].class);
                    List<ClientData> clientData = new ArrayList<>();
                    for (LoggedInUser user : users) {
                        clientData.addAll(Arrays.asList(user.getData()));
                    }
                    ((ClientDataListAdapter) adapter).addClientData(clientData);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
