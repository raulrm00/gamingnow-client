package org.zapto.raulrm00.gamingnow.ui.login.data.model;

import org.zapto.raulrm00.gamingnow.pairing.models.ClientData;

import java.io.Serializable;
import java.util.UUID;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser implements Serializable {

    private UUID id;
    private String name;
    private String email;
    private String password;
    private ClientData[] data;

    public LoggedInUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public ClientData[] getData() {
        return data;
    }
}