package org.zapto.raulrm00.gamingnow.ui.genres;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.rawg.models.Genre;
import org.zapto.raulrm00.gamingnow.rawg.util.APIGenresArrayGetter;
import org.zapto.raulrm00.gamingnow.util.ImageDownloader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GenresListAdapter extends RecyclerView.Adapter<GenresListAdapter.GenresHolder> {

    private List<Genre> generos;

    public GenresListAdapter() {
        generos = new ArrayList<>();
        init();
    }

    private void init() {
        try {
            generos.addAll(Arrays.asList(new APIGenresArrayGetter().execute().get()));
            notifyDataSetChanged();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public GenresHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.genre_row, parent, false);
        GenresHolder holder = new GenresHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GenresHolder holder, int position) {
        final Genre genre = generos.get(position);
        ImageDownloader.DownloadImageFromInternet(genre.getImageBackground(), holder.genre_image);
        holder.genre_title.setText(genre.getName());
        holder.genre_games.setText(genre.getGamesCount());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GenresResultActivity.class);
                intent.putExtra("slug", genre.getSlug());
                intent.putExtra("name", genre.getName());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return generos.size();
    }

    public static class GenresHolder extends RecyclerView.ViewHolder {

        public RelativeLayout layout;
        public ImageView genre_image;
        public TextView genre_title;
        public TextView genre_games;

        public GenresHolder(@NonNull RelativeLayout layout) {
            super(layout);
            this.layout = layout;
            genre_image = layout.findViewById(R.id.genre_image);
            genre_title = layout.findViewById(R.id.genre_title);
            genre_games = layout.findViewById(R.id.genre_games);
        }
    }
}
