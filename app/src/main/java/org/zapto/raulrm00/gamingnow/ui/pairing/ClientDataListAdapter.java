package org.zapto.raulrm00.gamingnow.ui.pairing;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.pairing.models.ClientData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClientDataListAdapter extends RecyclerView.Adapter<ClientDataListAdapter.ClientDataHolder> {

    private List<ClientData> data;

    public ClientDataListAdapter() {
        data = new ArrayList<>();
    }

    public ClientDataListAdapter(ClientData[] data) {
        this();
        Collections.addAll(this.data, data);
        notifyItemRangeInserted(0, data.length);
    }

    public ClientDataListAdapter(List<ClientData> data) {
        this();
        addClientData(data);
    }

    public void addClientData(List<ClientData> data) {
        this.data = data;
        notifyItemRangeInserted(0, this.data.size() - 1);
    }

    @NonNull
    @Override
    public ClientDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_data_row, parent, false);
        return new ClientDataListAdapter.ClientDataHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientDataHolder holder, int position) {
        final ClientData d = data.get(position);
        holder.client_data_username.setText(d.getUsername());
        holder.client_data_platform.setText(d.getPlatform());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ClientDataHolder extends RecyclerView.ViewHolder {

        public TextView client_data_username, client_data_platform;

        public ClientDataHolder(@NonNull RelativeLayout layout) {
            super(layout);
            client_data_username = layout.findViewById(R.id.client_data_username);
            client_data_platform = layout.findViewById(R.id.client_data_platform);
        }
    }
}
