package org.zapto.raulrm00.gamingnow.ui.genres;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.R;

public class GenresResultActivity extends AppCompatActivity {

    private RecyclerView gamesList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genres_result);
        Toolbar toolbar = findViewById(R.id.genres_result_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getIntent().getStringExtra("name"));
        gamesList = findViewById(R.id.genre_games);
        adapter = new GenresResultListAdapter(gamesList, getIntent().getStringExtra("slug"));
        layoutManager = new GridLayoutManager(this, getResources().getInteger(R.integer.columns));
        gamesList.setAdapter(adapter);
        gamesList.setLayoutManager(layoutManager);
    }
}
