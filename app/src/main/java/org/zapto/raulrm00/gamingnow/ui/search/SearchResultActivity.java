package org.zapto.raulrm00.gamingnow.ui.search;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.R;

public class SearchResultActivity extends AppCompatActivity {

    private RecyclerView gamesList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Toolbar toolbar = findViewById(R.id.search_result_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.search_result_title) + " "
                + getIntent().getStringExtra("original"));
        gamesList = findViewById(R.id.search_games);
        adapter = new SearchResultListAdapter(gamesList, getIntent().getStringExtra("query"));
        layoutManager = new GridLayoutManager(this, getResources().getInteger(R.integer.columns));
        gamesList.setAdapter(adapter);
        gamesList.setLayoutManager(layoutManager);
    }
}
