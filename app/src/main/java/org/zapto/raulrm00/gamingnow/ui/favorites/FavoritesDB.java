package org.zapto.raulrm00.gamingnow.ui.favorites;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class FavoritesDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FavoriteGames.db";

    public FavoritesDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRY);
        onCreate(db);
    }

    /**
     * Devuelve un arreglo de slugs de los juegos guardados.
     *
     * @return Arreglo de juegos guardados en favoritos
     */
    public String[] getGames() {
        String[] projection = {FavoriteContract.FavoriteEntry.COLUMN_SLUG};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                FavoriteContract.FavoriteEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                FavoriteContract.FavoriteEntry._ID + " DESC"
        );
        String[] slug = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            slug[i] = cursor.getString(
                    cursor.getColumnIndexOrThrow(FavoriteContract.FavoriteEntry.COLUMN_SLUG));
            i++;
        }
        cursor.close();
        return slug;
    }

    /**
     * Comprueba si un juego ya está en la lista de favoritos
     *
     * @param slug Slug del juego
     * @return Si el juego ya está en favoritos
     */
    public boolean isFavorite(String slug) {
        String selection = FavoriteContract.FavoriteEntry.COLUMN_SLUG + " = ?";
        String[] selectionArgs = {slug};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                FavoriteContract.FavoriteEntry.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);
        int filas = cursor.getCount();
        cursor.close();
        return filas > 0;
    }

    /**
     * Guarda un juego como favorito.
     *
     * @param slug Slug del juego
     * @return Clave primaria de la nueva fila o -1 si error
     */
    public long save(String slug) {
        ContentValues values = new ContentValues();
        values.put(FavoriteContract.FavoriteEntry.COLUMN_SLUG, slug);
        SQLiteDatabase db = getWritableDatabase();
        return db.insert(FavoriteContract.FavoriteEntry.TABLE_NAME, null, values);
    }

    /**
     * Borra un juego de favoritos.
     *
     * @param slug Juegos a ser eliminados
     * @return Número de filas borradas
     */
    public int delete(String... slug) {
        String selection = FavoriteContract.FavoriteEntry.COLUMN_SLUG + " = ?";
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(FavoriteContract.FavoriteEntry.TABLE_NAME, selection, slug);
    }

    // Cadena preparada para crear o actualizar la base de datos
    private static final String SQL_CREATE_ENTRY =
            "CREATE TABLE " + FavoriteContract.FavoriteEntry.TABLE_NAME + " ("
                    + FavoriteContract.FavoriteEntry._ID + " INTEGER PRIMARY KEY,"
                    + FavoriteContract.FavoriteEntry.COLUMN_SLUG + " TEXT UNIQUE)";

    // Cadena preparada para borrar la base de datos para upgrade o downgrade
    private static final String SQL_DELETE_ENTRY =
            "DROP TABLE IF EXISTS " + FavoriteContract.FavoriteEntry.TABLE_NAME;
}
