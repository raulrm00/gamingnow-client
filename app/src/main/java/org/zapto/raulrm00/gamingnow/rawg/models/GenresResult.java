package org.zapto.raulrm00.gamingnow.rawg.models;

public class GenresResult {
    private int count;
    private String previous;
    private String next;
    private Genre[] results;

    public Genre[] getResults() {
        return results;
    }
}
