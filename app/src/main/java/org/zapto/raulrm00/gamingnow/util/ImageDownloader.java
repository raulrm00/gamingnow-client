package org.zapto.raulrm00.gamingnow.util;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageDownloader {
    private ImageDownloader() {
    }

    public static void DownloadImageFromInternet(String path, ImageView image) {
        Picasso.get().load(path).fit().into(image);
    }
}
