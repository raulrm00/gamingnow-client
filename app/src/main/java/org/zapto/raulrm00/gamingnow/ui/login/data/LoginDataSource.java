package org.zapto.raulrm00.gamingnow.ui.login.data;

import org.zapto.raulrm00.gamingnow.ui.login.data.model.LoggedInUser;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {
        LoggedInUser basicUser = new LoggedInUser(username, password);
        LoggedInUser user = tryLogin(basicUser);
        if (user == null || user.getId() == null) {
            return new Result.Error(new IOException("Username or password not valid"));
        }
        return new Result.Success<>(user);
    }

    public void logout() {
        // TODO: revoke authentication
    }

    private LoggedInUser tryLogin(LoggedInUser basicUser) {
        try {
            AsyncLogin request = new AsyncLogin();
            return request.execute(basicUser).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}