package org.zapto.raulrm00.gamingnow.ui.favorites;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.GameDetailsActivity;
import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.rawg.API;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;
import org.zapto.raulrm00.gamingnow.ui.home.HomeListAdapter;
import org.zapto.raulrm00.gamingnow.util.ImageDownloader;

import java.util.concurrent.ExecutionException;

public class FavoritesListAdapter extends RecyclerView.Adapter<HomeListAdapter.GameHolder> {

    private Game[] games;

    FavoritesListAdapter(FavoritesDB db) {
        try {
            String[] slug = db.getGames();
            games = new Game[slug.length];
            for (int i = 0; i < slug.length; i++) {
                games[i] = new APIGameFavoriteGetter(slug[i]).execute().get();
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public HomeListAdapter.GameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_row, parent, false);
        HomeListAdapter.GameHolder holder = new HomeListAdapter.GameHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.GameHolder holder, int position) {
        final Game game = games[position];
        ImageDownloader.DownloadImageFromInternet(game.getImageUrl(), holder.game_image);
        holder.game_title.setText(game.getName());
        holder.game_genres.setText(game.getGenresString());
        holder.game_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GameDetailsActivity.class);
                intent.putExtra("slug", game.getSlug());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return games.length;
    }

    private static class APIGameFavoriteGetter extends AsyncTask<Void, Void, Game> {

        private String slug;

        APIGameFavoriteGetter(String slug) {
            this.slug = slug;
        }

        @Override
        protected Game doInBackground(Void... voids) {
            return API.getGame(slug);
        }
    }
}
