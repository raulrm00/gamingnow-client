package org.zapto.raulrm00.gamingnow.rawg.util;

import android.os.AsyncTask;

import org.zapto.raulrm00.gamingnow.rawg.API;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class APISearchGenreArrayGetter extends AsyncTask<Void, Void, Game[]> {

    private int page_size;
    private int page;
    private String query;

    public APISearchGenreArrayGetter(int page_size, int page, String query) {
        this.page_size = page_size;
        this.page = page;
        this.query = query;
    }

    @Override
    protected Game[] doInBackground(Void... voids) {
        return API.getGamesFromGenre(page_size, page, query);
    }

    public void next(List<Game> gameList) {
        try {
            gameList.addAll(Arrays.asList(execute().get()));
            page++;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
