package org.zapto.raulrm00.gamingnow.pairing.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;

import androidx.annotation.Nullable;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class PairingPOSTIntentService extends IntentService {

    private static final String TAG = PairingGETIntentService.class.getSimpleName();

    public static final String PENDING_RESULT_EXTRA = "pending_result";
    public static final String URL_EXTRA = "url";
    public static final String WRITE_DATA = "data";

    public static final int RESULT_CODE = 20;
    public static final int ERROR_CODE = 21;

    public PairingPOSTIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        PendingIntent reply = intent.getParcelableExtra(PENDING_RESULT_EXTRA);
        URL url;
        HttpURLConnection connection = null;
        try {
            try {
                url = new URL(intent.getStringExtra(URL_EXTRA));
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setChunkedStreamingMode(0);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("User-Agent", "Gaming Now!/0.1 (Android)");
                OutputStream out = new BufferedOutputStream(connection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8));
                writer.write(intent.getStringExtra(WRITE_DATA));
                writer.flush();
                writer.close();
                out.close();
                if (connection.getResponseCode() != 200) {
                    throw new RuntimeException("ERROR " + connection.getResponseCode());
                }
                reply.send(this, RESULT_CODE, new Intent());
            } catch (RuntimeException | IOException e) {
                e.printStackTrace();
                reply.send(ERROR_CODE);
            }
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

}
