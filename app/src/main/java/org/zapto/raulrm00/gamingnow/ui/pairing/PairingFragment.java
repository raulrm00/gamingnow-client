package org.zapto.raulrm00.gamingnow.ui.pairing;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.pairing.models.Room;
import org.zapto.raulrm00.gamingnow.pairing.services.PairingGETIntentService;
import org.zapto.raulrm00.gamingnow.ui.login.LoginActivity;

public class PairingFragment extends Fragment {

    private static final String PAIRING_SERVER_IP = "raulrm00.zapto.org";
    private static final String STORED_CLIENT_ROOMS = String.format("http://%s/api/clients/%s/rooms", PAIRING_SERVER_IP, LoginActivity.user.getId());

    public static final int PAIRING_REQUEST_CODE = 9;

    private RecyclerView roomsList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_pairing, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        roomsList = view.findViewById(R.id.rooms);
        adapter = new RoomListAdapter();
        layoutManager = new LinearLayoutManager(getContext());
        roomsList.setAdapter(adapter);
        roomsList.setLayoutManager(layoutManager);
        getDataFromServer();
    }

    public void getDataFromServer() {
        PendingIntent pendingResult = getActivity().createPendingResult(
                PAIRING_REQUEST_CODE, new Intent(), 0);
        Intent intent = new Intent(getContext(), PairingGETIntentService.class);
        intent.putExtra(PairingGETIntentService.URL_EXTRA, STORED_CLIENT_ROOMS);
        intent.putExtra(PairingGETIntentService.PENDING_RESULT_EXTRA, pendingResult);
        getActivity().startService(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAIRING_REQUEST_CODE) {
            switch (resultCode) {
                case PairingGETIntentService.ERROR_CODE:
                    Toast.makeText(getContext(), getString(R.string.room_details_error), Toast.LENGTH_LONG);
                    break;
                case PairingGETIntentService.RESULT_CODE:
                    Room[] roomList = new Gson().fromJson(data.getStringExtra(PairingGETIntentService.JSON_RESULT_EXTRA), Room[].class);
                    ((RoomListAdapter) adapter).addRooms(roomList);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
