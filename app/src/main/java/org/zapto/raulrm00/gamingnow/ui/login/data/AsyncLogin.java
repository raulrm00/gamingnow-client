package org.zapto.raulrm00.gamingnow.ui.login.data;

import android.os.AsyncTask;

import com.google.gson.Gson;

import org.zapto.raulrm00.gamingnow.ui.login.data.model.LoggedInUser;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class AsyncLogin extends AsyncTask<LoggedInUser, Void, LoggedInUser> {

    @Override
    protected LoggedInUser doInBackground(LoggedInUser... basicUser) {
        String json;
        URL url;
        HttpURLConnection connection = null;
        try {
            try {
                url = new URL("http://raulrm00.zapto.org/api/clients/login");
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setChunkedStreamingMode(0);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("User-Agent", "Gaming Now!/0.1 (Android)");
                OutputStream out = new BufferedOutputStream(connection.getOutputStream());
                BufferedWriter req = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8));
                req.write(new Gson().toJson(basicUser[0]));
                req.flush();
                req.close();
                out.close();
                if (connection.getResponseCode() != 200) {
                    throw new RuntimeException("ERROR " + connection.getResponseCode());
                }
                InputStream in = connection.getInputStream();
                json = new Scanner(in, "UTF-8").useDelimiter("\\Z").next();
                return new Gson().fromJson(json, LoggedInUser.class);
            } catch (RuntimeException | IOException e) {
                e.printStackTrace();
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

}
