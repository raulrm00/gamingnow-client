package org.zapto.raulrm00.gamingnow.rawg.util;

import android.os.AsyncTask;

import org.zapto.raulrm00.gamingnow.rawg.API;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class APIGameArrayGetter extends AsyncTask<Void, Void, Game[]> {

    private int page_size;
    private int page;

    public APIGameArrayGetter(int page_size, int page) {
        this.page_size = page_size;
        this.page = page;
    }

    @Override
    protected Game[] doInBackground(Void... voids) {
        return API.getMostPopularGamesFromYear(2020, page_size, page);
    }

    public void next(List<Game> gameList) {
        try {
            execute();
            gameList.addAll(Arrays.asList(get()));
            page++;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
