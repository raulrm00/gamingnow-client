package org.zapto.raulrm00.gamingnow.pairing.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class PairingGETIntentService extends IntentService {

    private static final String TAG = PairingGETIntentService.class.getSimpleName();

    public static final String PENDING_RESULT_EXTRA = "pending_result";
    public static final String URL_EXTRA = "url";
    public static final String JSON_RESULT_EXTRA = "json";

    public static final int RESULT_CODE = 10;
    public static final int ERROR_CODE = 11;

    public PairingGETIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        PendingIntent reply = intent.getParcelableExtra(PENDING_RESULT_EXTRA);
        String json;
        URL url;
        HttpURLConnection connection = null;
        try {
            try {
                url = new URL(intent.getStringExtra(URL_EXTRA));
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("User-Agent", "Gaming Now!/0.1 (Android)");
                if (connection.getResponseCode() != 200) {
                    throw new RuntimeException("ERROR " + connection.getResponseCode());
                }
                InputStream inStream = connection.getInputStream();
                json = new Scanner(inStream, "UTF-8").useDelimiter("\\Z").next();
                Intent result = new Intent();
                result.putExtra(JSON_RESULT_EXTRA, json);
                reply.send(this, RESULT_CODE, result);
            } catch (RuntimeException | IOException e) {
                e.printStackTrace();
                reply.send(ERROR_CODE);
            }
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

}
