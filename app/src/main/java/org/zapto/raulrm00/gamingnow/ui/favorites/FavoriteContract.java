package org.zapto.raulrm00.gamingnow.ui.favorites;

import android.provider.BaseColumns;

public final class FavoriteContract {
    private FavoriteContract() {
    }

    public static class FavoriteEntry implements BaseColumns {
        public static final String TABLE_NAME = "FAVORITES";
        public static final String COLUMN_SLUG = "SLUG";
    }
}
