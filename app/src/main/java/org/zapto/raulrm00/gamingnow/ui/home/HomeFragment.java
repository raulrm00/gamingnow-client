package org.zapto.raulrm00.gamingnow.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.R;

public class HomeFragment extends Fragment {

    private RecyclerView gamesList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gamesList = getView().findViewById(R.id.games);
        adapter = new HomeListAdapter(gamesList);
        layoutManager = new GridLayoutManager(getContext(), getResources().getInteger(R.integer.columns));
        gamesList.setAdapter(adapter);
        gamesList.setLayoutManager(layoutManager);
    }
}
