package org.zapto.raulrm00.gamingnow.ui.home;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.zapto.raulrm00.gamingnow.GameDetailsActivity;
import org.zapto.raulrm00.gamingnow.R;
import org.zapto.raulrm00.gamingnow.rawg.models.Game;
import org.zapto.raulrm00.gamingnow.rawg.util.APIGameArrayGetter;
import org.zapto.raulrm00.gamingnow.util.ImageDownloader;

import java.util.ArrayList;
import java.util.List;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.GameHolder> {

    private RecyclerView lista;

    private List<Game> juegos;
    private int page_size;
    private int page;

    public HomeListAdapter(RecyclerView lista) {
        this.lista = lista;
        juegos = new ArrayList<>();
        page_size = 10;
        page = 1;
        init();
    }

    private void init() {
        new APIGameArrayGetter(page_size, page).next(juegos);
        notifyItemRangeInserted(0, 10);
        page++;
    }

    @NonNull
    @Override
    public GameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_row, parent, false);
        GameHolder holder = new GameHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GameHolder holder, final int position) {
        if (position == getItemCount() - 1) {
            new APIGameArrayGetter(page_size, page).next(juegos);
            lista.post(new Runnable() {
                @Override
                public void run() {
                    notifyItemRangeInserted(position, juegos.size() - position - 1);
                }
            });
            page++;
        }
        final Game game = juegos.get(position);
        ImageDownloader.DownloadImageFromInternet(game.getImageUrl(), holder.game_image);
        holder.game_title.setText(game.getName());
        holder.game_genres.setText(game.getGenresString());
        holder.game_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GameDetailsActivity.class);
                intent.putExtra("slug", game.getSlug());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return juegos.size();
    }

    public static class GameHolder extends RecyclerView.ViewHolder {

        public ImageView game_image;
        public TextView game_title;
        public TextView game_genres;
        public CardView game_card;

        public GameHolder(@NonNull RelativeLayout layout) {
            super(layout);
            game_image = layout.findViewById(R.id.game_image);
            game_title = layout.findViewById(R.id.game_title);
            game_genres = layout.findViewById(R.id.game_genres);
            game_card = layout.findViewById(R.id.game_card);
        }
    }

}
